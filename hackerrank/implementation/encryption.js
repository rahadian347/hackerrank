process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();    
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function main() {
    var s = readLine();
    var n = s.length;
    var result = '';
    var row = Math.round(Math.sqrt(n));
    var column;
    if (row >= Math.sqrt(n)) {
        column = row;
    } else {
        column = row+1;
    }
    for (var i = 0; i < column; i++) {
        var j = i;
        while (j < n) {
            result += s[j];
            j += column;
        }
        result += ' ';
    }
    console.log(result);
}


